package com.hirbod.upsreader;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.Keep;

import java.text.DecimalFormat;

/**
 * Created by KhaledLela on 9/27/17.
 */

public abstract class BaseProgress extends View {
    /**
     * Reports the range which the current value is in.
     */
    public enum ValueRange {
        HIGH,
        NORMAL,
        LOW,
    }

    /**
     * decimalformat for formatting the progress
     */
    private DecimalFormat mFormat;
    private float progress;
    private float max = 100, min = 0;
    private float high = 100, low = 0;
    private ObjectAnimator objectAnimator;

    /**
     * the number of decimal digits this formatter uses
     */
    protected int digits = 0;

    public BaseProgress(Context context) {
        super(context);
    }

    public BaseProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    abstract void initByAttributes(TypedArray attributes);

    public void setProgressFloating(int digits) {
        this.digits = digits;
        setFormatter();
    }

    protected void setFormatter() {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < digits; i++) {
            if (i == 0)
                b.append(".");
            b.append("0");
        }
        mFormat = new DecimalFormat("###,###,###,##0" + b);
    }

    /**
     * Set progress with animation.
     *
     * @param progress end progress
     * @param duration time for animation.
     */
    public void setProgressWithAnimation(float progress, int duration) {
        setProgressWithAnimation(getProgress(), progress, duration, null);
    }

    public void setProgressWithAnimation(float progress, int duration, Animator.AnimatorListener listener) {
        setProgressWithAnimation(getProgress(), progress, duration, listener);
    }

    public void setProgressWithAnimation(float startProgress, float endProgress, int duration, Animator.AnimatorListener listener) {
        cancelAnimation(); // Clear previous animation if exist.
        objectAnimator = ObjectAnimator.ofFloat(this, "progress", startProgress, endProgress);
        objectAnimator.setDuration(duration);
        if (listener != null)
            objectAnimator.addListener(listener);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
    }


    public void cancelAnimation() {
        if (objectAnimator != null && objectAnimator.isRunning())
            objectAnimator.cancel();
    }

    @Keep
    public void setProgress(float progress) {
        this.progress = Math.max(getMin(), Math.min(getMax(), progress));
        invalidate();
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        if (max > this.min) {
            this.max = max;
            invalidate();
        }
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        if (this.max > min) {
            this.min = min;
            invalidate();
        }
    }

    public float getLow() {
        return low;
    }

    public void setLow(float low) {
        if (this.high > low) {
            this.low = low;
            invalidate();
        }
    }

    public float getHigh() {
        return high;
    }

    public void setHigh(float high) {
        if (high > this.low) {
            this.high = high;
            invalidate();
        }
    }

    public float getProgress() {
        return progress - getMin();
    }

    public float getRawProgress() {
        return progress;
    }

    public String getFormattedProgress() {
        return mFormat.format(getRawProgress());
    }

    /**
     * Gets the current range of progress bar.
     */
    public ValueRange getRange() {
        if (progress < low)
            return ValueRange.LOW;
        if (progress > high)
            return ValueRange.HIGH;
        return ValueRange.NORMAL;
    }
}
