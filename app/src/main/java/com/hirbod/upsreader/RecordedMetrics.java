package com.hirbod.upsreader;

import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;

public class RecordedMetrics {
    private final static String LOG_TAG = "recorded_metric";
    private final String createdDate = Utils.getUnixTime();
    private String serialNumber;
    private BigDecimal voltageOut;
    private BigDecimal voltageIn;
    private BigDecimal currentOutput;
    private BigDecimal batteryVoltage;
    private BigDecimal frequency;
    private BigDecimal heatsink;

    public RecordedMetrics(String data) {
        Log.v(LOG_TAG, "Parsing " + data);
        final String[] lines = data.split("\\r?\\n");
        for (String line : lines) {
            line = line.trim();
            if (line.startsWith("SN="))
                serialNumber = line.substring(3);
            else if (line.startsWith("Vo="))
                voltageOut = new BigDecimal(line.substring(3));
            else if (line.startsWith("Vi="))
                voltageIn = new BigDecimal(line.substring(3));
            else if (line.startsWith("Io="))
                currentOutput = new BigDecimal(line.substring(3)).scaleByPowerOfTen(-3);
            else if (line.startsWith("V_Bat="))
                batteryVoltage = new BigDecimal(line.substring(6)).scaleByPowerOfTen(-2);
            else if (line.startsWith("Frq="))
                frequency = new BigDecimal(line.substring(4)).scaleByPowerOfTen(-2);
            else if (line.startsWith("H_T "))
                heatsink = new BigDecimal(line.substring(4));
        }
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public BigDecimal getVoltageOut() {
        return voltageOut;
    }

    public BigDecimal getVoltageIn() {
        return voltageIn;
    }

    public BigDecimal getCurrentOutput() {
        return currentOutput;
    }

    public BigDecimal getBatteryVoltage() {
        return batteryVoltage;
    }

    public BigDecimal getFrequency() {
        return frequency;
    }

    public BigDecimal getHeatsink() {
        return heatsink;
    }

    /**
     * Writes the data of this recorded metric into a log file
     *
     * @param out The log to write the data in it
     */
    public void write(OutputStream out) {
        if (out == null)
            return;
        try {
            writeEntry(out, getSerialNumber(), "SerialNumber");
            writeEntry(out, getVoltageOut(), "VoltageOut");
            writeEntry(out, getVoltageIn(), "VoltageIn");
            writeEntry(out, getCurrentOutput(), "CurrentOutput");
            writeEntry(out, getBatteryVoltage(), "BatteryVoltage");
            writeEntry(out, getFrequency(), "Frequency");
            writeEntry(out, getHeatsink(), "Heatsink");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeEntry(OutputStream appendable, Object object, String name) throws IOException {
        if (object == null)
            return;
        appendable.write(String.format("%s,%s,%s\n", createdDate, name, object).getBytes());
    }
}
