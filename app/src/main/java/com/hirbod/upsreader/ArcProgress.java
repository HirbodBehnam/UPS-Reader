package com.hirbod.upsreader;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by bruce on 11/6/14.
 * Edited by Hirbod
 */
public class ArcProgress extends BaseProgress {
    public static final int UNFINISHED_STOKE_COLOR = Color.WHITE;
    public static final int DEFAULT_TEXT_COLOR = Color.WHITE;
    public static final int DEFAULT_HIGH_COLOR = Color.rgb(0xF4, 0x43, 0x36);
    public static final int DEFAULT_NORMAL_COLOR = Color.rgb(0x66, 0xBB, 0x6A);
    public static final int DEFAULT_LOW_COLOR = DEFAULT_HIGH_COLOR; // Color.rgb(0x03, 0xA9, 0xF4);
    public static final float ARC_ANGLE = 250;
    private final Paint paint = new Paint();
    protected final Paint textPaint = new TextPaint();

    private final RectF rectF = new RectF();

    private float strokeWidth;
    private float suffixTextSize;
    private float bottomTextSize;
    private String bottomText = "";
    private float textSize;
    private int textColor;
    /**
     * Colors of arc based on the value of progress
     */
    private int lowColor, normalColor, highColor;
    private String suffixText = "%";
    private float suffixTextPadding;
    private float arcBottomHeight;
    private final float default_suffix_text_size;
    private final float default_suffix_padding;
    private final float default_bottom_text_size;
    private final float default_stroke_width;
    private final static String DEFAULT_SUFFIX_TEXT = "%";
    private static final int default_max = 100, default_min = 0;
    private static final int default_high = 100, default_low = 0;
    private float default_text_size;
    private final int min_size;

    private static final String INSTANCE_STATE = "saved_instance";
    private static final String INSTANCE_STROKE_WIDTH = "stroke_width";
    private static final String INSTANCE_SUFFIX_TEXT_SIZE = "suffix_text_size";
    private static final String INSTANCE_SUFFIX_TEXT_PADDING = "suffix_text_padding";
    private static final String INSTANCE_BOTTOM_TEXT_SIZE = "bottom_text_size";
    private static final String INSTANCE_BOTTOM_TEXT = "bottom_text";
    private static final String INSTANCE_TEXT_SIZE = "text_size";
    private static final String INSTANCE_TEXT_COLOR = "text_color";
    private static final String INSTANCE_PROGRESS = "progress";
    private static final String INSTANCE_MAX = "max";
    private static final String INSTANCE_MIN = "min";
    private static final String INSTANCE_LOW = "low";
    private static final String INSTANCE_HIGH = "high";
    private static final String INSTANCE_LOW_COLOR = "low_color";
    private static final String INSTANCE_NORMAL_COLOR = "normal_color";
    private static final String INSTANCE_HIGH_COLOR = "high_color";
    private static final String INSTANCE_SUFFIX = "suffix";
    private static final String LOG_TAG = "ArcProgress";

    public ArcProgress(Context context) {
        this(context, null);
    }

    public ArcProgress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArcProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        default_text_size = Utils.sp2px(getResources(), 18);
        min_size = (int) Utils.dp2px(getResources(), 100);
        default_text_size = Utils.sp2px(getResources(), 40);
        default_suffix_text_size = Utils.sp2px(getResources(), 15);
        default_suffix_padding = Utils.dp2px(getResources(), 4);
        default_bottom_text_size = Utils.sp2px(getResources(), 16);
        default_stroke_width = Utils.dp2px(getResources(), 7);

        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ArcProgress, defStyleAttr, 0);
        initByAttributes(attributes);
        attributes.recycle();

        initPainters();
        logInit();
    }

    protected void initByAttributes(TypedArray attributes) {
        lowColor = attributes.getColor(R.styleable.ArcProgress_arc_low_color, DEFAULT_LOW_COLOR);
        normalColor = attributes.getColor(R.styleable.ArcProgress_arc_normal_color, DEFAULT_NORMAL_COLOR);
        highColor = attributes.getColor(R.styleable.ArcProgress_arc_high_color, DEFAULT_HIGH_COLOR);
        textColor = attributes.getColor(R.styleable.ArcProgress_arc_text_color, DEFAULT_TEXT_COLOR);
        textSize = attributes.getDimension(R.styleable.ArcProgress_arc_text_size, default_text_size);
        setMax(attributes.getFloat(R.styleable.ArcProgress_arc_max, default_max));
        setMin(attributes.getFloat(R.styleable.ArcProgress_arc_min, default_min));
        setHigh(attributes.getFloat(R.styleable.ArcProgress_arc_high, default_high));
        setLow(attributes.getFloat(R.styleable.ArcProgress_arc_low, default_low));
        setProgress(attributes.getFloat(R.styleable.ArcProgress_arc_progress, 0));
        strokeWidth = attributes.getDimension(R.styleable.ArcProgress_arc_stroke_width, default_stroke_width);
        suffixTextSize = attributes.getDimension(R.styleable.ArcProgress_arc_suffix_text_size, default_suffix_text_size);
        suffixText = TextUtils.isEmpty(attributes.getString(R.styleable.ArcProgress_arc_suffix_text)) ? DEFAULT_SUFFIX_TEXT : attributes.getString(R.styleable.ArcProgress_arc_suffix_text);
        suffixTextPadding = attributes.getDimension(R.styleable.ArcProgress_arc_suffix_text_padding, default_suffix_padding);
        bottomTextSize = attributes.getDimension(R.styleable.ArcProgress_arc_bottom_text_size, default_bottom_text_size);
        bottomText = attributes.getString(R.styleable.ArcProgress_arc_bottom_text);

        digits = attributes.getInt(R.styleable.ArcProgress_arc_progress_digits, 0);
        setFormatter();
    }

    protected void initPainters() {
        textPaint.setColor(textColor);
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);

        paint.setColor(UNFINISHED_STOKE_COLOR);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    public void invalidate() {
        initPainters();
        super.invalidate();
    }

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        this.invalidate();
    }

    public float getSuffixTextSize() {
        return suffixTextSize;
    }

    public void setSuffixTextSize(float suffixTextSize) {
        this.suffixTextSize = suffixTextSize;
        this.invalidate();
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
        this.invalidate();
    }

    public float getBottomTextSize() {
        return bottomTextSize;
    }

    public void setBottomTextSize(float bottomTextSize) {
        this.bottomTextSize = bottomTextSize;
        this.invalidate();
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        this.invalidate();
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        this.invalidate();
    }

    public String getSuffixText() {
        return suffixText;
    }

    public void setSuffixText(String suffixText) {
        this.suffixText = suffixText;
        this.invalidate();
    }

    public float getSuffixTextPadding() {
        return suffixTextPadding;
    }

    public void setSuffixTextPadding(float suffixTextPadding) {
        this.suffixTextPadding = suffixTextPadding;
        this.invalidate();
    }

    private void setPaintColorBasedOnValue() {
        switch (super.getRange()) {
            case LOW -> paint.setColor(lowColor);
            case NORMAL -> paint.setColor(normalColor);
            case HIGH -> paint.setColor(highColor);
        }
    }

    @Override
    protected int getSuggestedMinimumHeight() {
        return min_size;
    }

    @Override
    protected int getSuggestedMinimumWidth() {
        return min_size;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        rectF.set(strokeWidth / 2f, strokeWidth / 2f, width - strokeWidth / 2f, MeasureSpec.getSize(heightMeasureSpec) - strokeWidth / 2f);
        float radius = width / 2f;
        float angle = (360 - ARC_ANGLE) / 2f;
        arcBottomHeight = radius * (1 - Utils.cosDegree(angle));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float startAngle = 270 - ARC_ANGLE / 2f;
        float finishedSweepAngle = getProgress() / getMax() * ARC_ANGLE;
        float finishedStartAngle = startAngle;
        if (getProgress() == 0) finishedStartAngle = 0.01f;
        paint.setColor(UNFINISHED_STOKE_COLOR);
        canvas.drawArc(rectF, startAngle, ARC_ANGLE, false, paint);
        setPaintColorBasedOnValue();
        canvas.drawArc(rectF, finishedStartAngle, finishedSweepAngle, false, paint);

        String text = getFormattedProgress();
        if (!TextUtils.isEmpty(text)) {
            textPaint.setColor(textColor);
            textPaint.setTextSize(textSize);
            float textHeight = textPaint.descent() + textPaint.ascent();
            float textBaseline = (getHeight() - textHeight) / 2.0f;
            canvas.drawText(text, (getWidth() - textPaint.measureText(text)) / 2.0f, textBaseline, textPaint);
            textPaint.setTextSize(suffixTextSize);
            float suffixHeight = textPaint.descent() + textPaint.ascent();
            canvas.drawText(suffixText, getWidth() / 2.0f + textPaint.measureText(text) + suffixTextPadding, textBaseline + textHeight - suffixHeight, textPaint);
        }

        if (arcBottomHeight == 0) {
            float radius = getWidth() / 2f;
            float angle = (360 - ARC_ANGLE) / 2f;
            arcBottomHeight = radius * (1 - Utils.cosDegree(angle));
        }

        if (!TextUtils.isEmpty(getBottomText())) {
            // Fix big font on small windows
            float bottomTextSizeFinal = getTextSizeForWidth(paint, bottomTextSize, maxButtonTextWidth(), getBottomText());
            textPaint.setTextSize(Math.min(bottomTextSizeFinal, bottomTextSize));
            float bottomTextBaseline = getHeight() - arcBottomHeight - (textPaint.descent() + textPaint.ascent()) / 2;
            canvas.drawText(getBottomText(), (getWidth() - textPaint.measureText(getBottomText())) / 2.0f, bottomTextBaseline, textPaint);
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(INSTANCE_STATE, super.onSaveInstanceState());
        bundle.putFloat(INSTANCE_STROKE_WIDTH, getStrokeWidth());
        bundle.putFloat(INSTANCE_SUFFIX_TEXT_SIZE, getSuffixTextSize());
        bundle.putFloat(INSTANCE_SUFFIX_TEXT_PADDING, getSuffixTextPadding());
        bundle.putFloat(INSTANCE_BOTTOM_TEXT_SIZE, getBottomTextSize());
        bundle.putString(INSTANCE_BOTTOM_TEXT, getBottomText());
        bundle.putFloat(INSTANCE_TEXT_SIZE, getTextSize());
        bundle.putInt(INSTANCE_TEXT_COLOR, getTextColor());
        bundle.putFloat(INSTANCE_PROGRESS, getRawProgress());
        bundle.putFloat(INSTANCE_MAX, getMax());
        bundle.putFloat(INSTANCE_MIN, getMin());
        bundle.putFloat(INSTANCE_LOW, getLow());
        bundle.putFloat(INSTANCE_HIGH, getHigh());
        bundle.putInt(INSTANCE_LOW_COLOR, lowColor);
        bundle.putInt(INSTANCE_NORMAL_COLOR, normalColor);
        bundle.putInt(INSTANCE_HIGH_COLOR, highColor);
        bundle.putString(INSTANCE_SUFFIX, getSuffixText());
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof final Bundle bundle) {
            strokeWidth = bundle.getFloat(INSTANCE_STROKE_WIDTH);
            suffixTextSize = bundle.getFloat(INSTANCE_SUFFIX_TEXT_SIZE);
            suffixTextPadding = bundle.getFloat(INSTANCE_SUFFIX_TEXT_PADDING);
            bottomTextSize = bundle.getFloat(INSTANCE_BOTTOM_TEXT_SIZE);
            bottomText = bundle.getString(INSTANCE_BOTTOM_TEXT);
            textSize = bundle.getFloat(INSTANCE_TEXT_SIZE);
            textColor = bundle.getInt(INSTANCE_TEXT_COLOR);
            setMax(bundle.getFloat(INSTANCE_MAX));
            setMin(bundle.getFloat(INSTANCE_MIN));
            setLow(bundle.getFloat(INSTANCE_LOW));
            setHigh(bundle.getFloat(INSTANCE_HIGH));
            lowColor = bundle.getInt(INSTANCE_LOW_COLOR);
            normalColor = bundle.getInt(INSTANCE_NORMAL_COLOR);
            highColor = bundle.getInt(INSTANCE_HIGH_COLOR);
            setProgress(bundle.getFloat(INSTANCE_PROGRESS));
            suffixText = bundle.getString(INSTANCE_SUFFIX);
            initPainters();
            super.onRestoreInstanceState(bundle.getParcelable(INSTANCE_STATE));
            return;
        }
        super.onRestoreInstanceState(state);
    }

    private void logInit() {
        String log = "Initialized with: \n";
        log += "ID: " + getId() + "\n";
        log += "Low: " + getLow() + "\n";
        log += "High: " + getHigh() + "\n";
        log += "Min: " + getMin() + "\n";
        log += "Max: " + getMax() + "\n";
        Log.v(LOG_TAG, log);
    }

    /**
     * There is a text box below the arc which shows a text below the arc. We can get the max width of it
     * using this method
     *
     * @return The max width of element
     */
    private float maxButtonTextWidth() {
        final float DEGREE_FIX = 30;
        final float width = getWidth();
        final float result = (float) Math.sqrt(width * width / 2 * (1 - Utils.cosDegree(360 - ARC_ANGLE - DEGREE_FIX)));
        //Log.v(LOG_TAG, "width " + width + " gave " + result);
        return result;
    }

    /**
     * Sets the text size for a Paint object so a given string of text will be a
     * given width.
     * From <a href="https://stackoverflow.com/a/21895626/4213397">this link</a>
     *
     * @param paint           the Paint to set the text size for
     * @param initialTextSize the initial text size to
     * @param desiredWidth    the desired width
     * @param text            the text that should be that width
     * @return The good text size
     */
    private static float getTextSizeForWidth(Paint paint, float initialTextSize, float desiredWidth,
                                             String text) {
        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(initialTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        return initialTextSize * desiredWidth / bounds.width();
    }
}
