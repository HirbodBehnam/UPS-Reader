package com.hirbod.upsreader;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private BluetoothDevice[] bluetoothDevices = new BluetoothDevice[0];
    private static final int REQUEST_BLUETOOTH_PERMISSION_CODE = 0;
    private final ActivityResultLauncher<Intent> defineLogLocationResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data == null)
                        return;
                    Uri uri = data.getData();
                    if (uri == null)
                        return;
                    getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    Log.i("LOGGER", "Selected " + uri.getPath() + " as location");
                    // Save it somewhere
                    Storage.saveLogPath(getApplicationContext(), uri);
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set the spinner
        setBluetoothDevices();
        // Set clicked listener
        findViewById(R.id.OpenBluetoothButton).setOnClickListener(v -> {
            Object selectedObject = ((Spinner) findViewById(R.id.BluetoothSelectorSpinner)).getSelectedItem();
            if (selectedObject == null)
                return;
            String name = selectedObject.toString();
            if (name.isEmpty()) {
                Toast.makeText(MainActivity.this, R.string.bluetooth_not_selected, Toast.LENGTH_SHORT).show();
                return;
            }
            Intent myIntent = new Intent(MainActivity.this, ReaderActivity.class);
            myIntent.putExtra(ReaderActivity.BLUETOOTH_EXTRA_NAME, bluetoothDevices[((Spinner) findViewById(R.id.BluetoothSelectorSpinner)).getSelectedItemPosition()]);
            MainActivity.this.startActivity(myIntent);
        });
    }

    private void setBluetoothDevices() {
        // Get bluetooth adaptor
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.bluetooth_no_adaptor, Toast.LENGTH_LONG).show();
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Toast.makeText(this, R.string.bluetooth_enable, Toast.LENGTH_LONG).show();
            return;
        }
        // Request access to bluetooth adaptor
        if (Build.VERSION.SDK_INT >= 31) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH_CONNECT}, REQUEST_BLUETOOTH_PERMISSION_CODE);
                return;
            }
        }
        // List all paired devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() == 0) {
            Toast.makeText(this, R.string.bluetooth_no_paired, Toast.LENGTH_LONG).show();
            return;
        }
        ArrayList<String> deviceNames = new ArrayList<>(pairedDevices.size());
        ArrayList<BluetoothDevice> devices = new ArrayList<>(pairedDevices.size());
        for (BluetoothDevice device : pairedDevices) {
            deviceNames.add(device.getName());
            devices.add(device);
        }
        bluetoothDevices = devices.toArray(new BluetoothDevice[0]);

        Spinner s = findViewById(R.id.BluetoothSelectorSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, deviceNames.toArray(new String[0]));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.ShowLogsButton) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            defineLogLocationResultLauncher.launch(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}