package com.hirbod.upsreader;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.documentfile.provider.DocumentFile;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReaderActivity extends AppCompatActivity {
    public static final String BLUETOOTH_EXTRA_NAME = "bt_name";
    private static final String LOG_TAG = "reader";
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private final AtomicBoolean stopWorker = new AtomicBoolean(false);
    private static final AtomicBoolean ACTIVITY_RUNNING = new AtomicBoolean(false);
    private OutputStream logger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ACTIVITY_RUNNING.set(true);
        setContentView(R.layout.activity_reader);
        // Write log headers
        try {
            Uri tree = Storage.loadLogPath(getApplicationContext());
            Log.v(LOG_TAG, "Loaded " + tree + " as tree");
            DocumentFile documentTree = DocumentFile.fromTreeUri(getApplicationContext(), tree);
            if (documentTree == null)
                throw new NullPointerException("documentTree");
            DocumentFile logFile = documentTree.createFile("text/csv", Utils.getUnixTime());
            if (logFile == null)
                throw new NullPointerException("logFile");
            Log.v(LOG_TAG, "Name: " + logFile.getName() + "; Can write: " + logFile.canWrite());
            logger = getContentResolver().openOutputStream(logFile.getUri(), "w");
            if (logger == null)
                throw new NullPointerException("logger");
            logger.write("Date,Type,Value\n".getBytes());
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, "Cannot write log headers.", Toast.LENGTH_SHORT).show();
            logger = new NullWriter();
        }
        // Get data
        mmDevice = getIntent().getParcelableExtra(BLUETOOTH_EXTRA_NAME);
        if (mmDevice == null) {
            finish();
            return;
        }
        // Start bluetooth
        new Thread(() -> {
            try {
                openBT();
            } catch (Exception ex) {
                ex.printStackTrace();
                runOnUiThread(() -> {
                    if (ACTIVITY_RUNNING.get())
                        new AlertDialog.Builder(ReaderActivity.this)
                                .setTitle("Error")
                                .setMessage(ex.getMessage())
                                .setPositiveButton("OK", (dialog, which) -> ReaderActivity.this.finish())
                                .show();
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ACTIVITY_RUNNING.set(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            closeBT();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // finalize log
        try {
            if (logger != null) {
                logger.flush();
                logger.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    void openBT() throws IOException {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            runOnUiThread(() -> {
                if (ACTIVITY_RUNNING.get())
                    Toast.makeText(ReaderActivity.this, R.string.bluetooth_not_permission, Toast.LENGTH_SHORT).show();
            });
            return;
        }
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
        Log.i(LOG_TAG, "Bluetooth Opened");
        beginListenForData();
    }

    void beginListenForData() {
        // Read data
        stopWorker.set(false);
        new Thread(() -> {
            while (!stopWorker.get()) {
                try {
                    final RecordedMetrics recordedMetrics = new RecordedMetrics(Utils.readUntilNewLine(mmInputStream));
                    recordedMetrics.write(logger); // log metrics
                    runOnUiThread(() -> { // update UI
                        if (!ACTIVITY_RUNNING.get())
                            return;
                        if (recordedMetrics.getSerialNumber() != null)
                            ((TextView) findViewById(R.id.SerialNumberResult)).setText(recordedMetrics.getSerialNumber());
                        if (recordedMetrics.getVoltageOut() != null)
                            ((ArcProgress) findViewById(R.id.VoltageOutput)).setProgress(recordedMetrics.getVoltageOut().floatValue());
                        if (recordedMetrics.getVoltageIn() != null)
                            ((ArcProgress) findViewById(R.id.VoltageInput)).setProgress(recordedMetrics.getVoltageIn().floatValue());
                        if (recordedMetrics.getCurrentOutput() != null)
                            ((ArcProgress) findViewById(R.id.CurrentOutput)).setProgress(recordedMetrics.getCurrentOutput().floatValue());
                        if (recordedMetrics.getBatteryVoltage() != null)
                            ((ArcProgress) findViewById(R.id.BatteryVoltage)).setProgress(recordedMetrics.getBatteryVoltage().floatValue());
                        if (recordedMetrics.getFrequency() != null)
                            ((ArcProgress) findViewById(R.id.Frequency)).setProgress(recordedMetrics.getFrequency().floatValue());
                        if (recordedMetrics.getHeatsink() != null)
                            ((ArcProgress) findViewById(R.id.Heatsink)).setProgress(recordedMetrics.getHeatsink().floatValue());
                    });
                } catch (IOException ex) {
                    ex.printStackTrace();
                    stopWorker.set(true);
                    break;
                }
            }
        }).start();
    }

    void closeBT() throws IOException {
        stopWorker.set(true);
        if (mmOutputStream != null)
            mmOutputStream.close();
        if (mmInputStream != null)
            mmInputStream.close();
        if (mmSocket != null)
            mmSocket.close();
        Log.i(LOG_TAG, "Bluetooth Closed");
    }
}