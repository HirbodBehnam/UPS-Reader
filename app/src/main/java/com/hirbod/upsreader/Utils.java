package com.hirbod.upsreader;

import android.content.res.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * Created by bruce on 14-11-6.
 */
public final class Utils {
    private Utils() {
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static String getUnixTime() {
        return Long.toString(new Date().getTime());
    }

    /**
     * Reads a buffer until the last char in buffer is a new line. It keeps reading the buffer until
     * the condition is met.
     *
     * @return The read data as string
     */
    public static String readUntilNewLine(InputStream stream) throws IOException {
        StringBuilder stringBuffer = new StringBuilder();
        byte[] buffer = new byte[1024 * 4];
        do {
            int bytesRead = stream.read(buffer);
            stringBuffer.append(new String(buffer, 0, bytesRead, StandardCharsets.US_ASCII));
        } while (stringBuffer.charAt(stringBuffer.length() - 1) != '\n');
        return stringBuffer.toString();
    }

    public static float cosDegree(float angle) {
        return (float) Math.cos(angle / 180 * Math.PI);
    }
}