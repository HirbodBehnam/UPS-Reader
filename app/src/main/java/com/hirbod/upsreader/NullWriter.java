package com.hirbod.upsreader;

import java.io.IOException;
import java.io.OutputStream;

public class NullWriter extends OutputStream {
    @Override
    public void write(int b) throws IOException {

    }
}
