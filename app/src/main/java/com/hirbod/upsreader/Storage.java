package com.hirbod.upsreader;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

public class Storage {
    private static final String PREFS_NAME = "ups-reader", LOG_PATH_PERF_NAME = "logPath";

    public static void saveLogPath(Context context, Uri logPath) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(LOG_PATH_PERF_NAME, logPath.toString());
        editor.apply();
    }

    public static Uri loadLogPath(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return Uri.parse(settings.getString(LOG_PATH_PERF_NAME, ""));
    }
}
